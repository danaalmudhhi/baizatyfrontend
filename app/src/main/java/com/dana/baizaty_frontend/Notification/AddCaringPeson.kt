package com.dana.baizaty_frontend.Notification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.fragments.PWCFragment
import com.dana.baizaty_frontend.singleton.AppSingleton
import kotlinx.android.synthetic.main.activity_add_caring_peson.*
import org.json.JSONObject

class AddCaringPeson : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_caring_peson)

        AddCPButton.setOnClickListener {
            val requestParams = HashMap<String,Any>()

            requestParams["name"] = NameEditText.getText().toString()
            requestParams["relationship"] = RelationEditText.getText().toString()
            requestParams["amount"] = AmountEditText.getText().toString()
            requestParams["email"] = EmailEditText.getText().toString()
            requestParams["phone"] = PhoneNoEditText.getText().toString()
            requestParams["status"] = 1
            requestParams["owner_id"] = 4
            val paramsJsonObject = JSONObject(requestParams as Map<*, *>)




            var queue = AppSingleton.instance.getQueue(this)

            var addUser = JsonObjectRequest(Request.Method.POST, AppSingleton.instance.getBaseUrl()
                    + "peopleWhoCare/addwhocare", paramsJsonObject, Response.Listener {

                // Success case
                Toast.makeText(this, "${NameEditText.getText().toString()} added !", Toast.LENGTH_LONG).show()

//                val intent = Intent(this, PWCFragment::class.java)
//                startActivity(intent)
                finish()
            }, Response.ErrorListener {
                // Error case
                Toast.makeText(this, "Internet connection error!", Toast.LENGTH_LONG).show()
            })

            queue?.add(addUser)
        }
    }
}
