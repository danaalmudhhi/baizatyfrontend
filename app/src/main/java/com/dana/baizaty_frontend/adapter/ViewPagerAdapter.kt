package com.dana.baizaty_frontend.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.dana.baizaty_frontend.fragments.Gargish
import com.dana.baizaty_frontend.fragments.HomeFragment
import com.dana.baizaty_frontend.fragments.PFM
import com.dana.baizaty_frontend.fragments.PWCFragment
import com.dana.baizaty_frontend.fragments.investmentFragment


class ViewPagerAdapter(fm:FragmentManager): FragmentPagerAdapter(fm) {


    override fun getItem(position: Int): Fragment {

        when (position) {

            0 -> return PWCFragment()
            1 -> return PFM()
            2 -> return HomeFragment()
            3 -> return Gargish()
            4 -> return investmentFragment()
        }

        return HomeFragment()
    }

    override fun getCount(): Int {

        return 5
    }


}