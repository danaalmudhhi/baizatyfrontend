package com.dana.baizaty_frontend.fragments


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.telephony.SmsManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.dana.baizaty_frontend.GargishActivity
import com.dana.baizaty_frontend.My_investment_Activity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.singleton.AppSingleton
import kotlinx.android.synthetic.main.fragment_investment.*
import kotlinx.android.synthetic.main.fragment_investment.view.*
import org.json.JSONObject


class investmentFragment :Fragment() {
    companion object{

        var OrganizationSelected=""
        var persentage=0.0
        var commitment=0
        var amountToInvest=0.0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_investment, container, false)
        // Spinner element
//////////////////////////////////////////////////////////////////////
        v?.button_My_INVEST?.setOnClickListener {
            val intent = Intent(activity, My_investment_Activity::class.java)
            startActivity(intent)
        }
        //////////////////////////////////////////////////////////////////////



        v?.button2_calculate?.setOnClickListener {

            OrganizationSelected =OrgList.selectedItem.toString()

            if(editTextAmount.text.toString()==""||editTextAmount.text.toString().toDouble()<50.0) {

                if(editTextAmount.text.toString()=="")
                Toast.makeText(this.context, "Please set an amount to invest", Toast.LENGTH_LONG).show()
                else
                Toast.makeText(this.context, "minimum amount to invest 50 kd", Toast.LENGTH_LONG).show()
            }
            else{

                amountToInvest = editTextAmount.text.toString().toDouble()

                if (OrganizationSelected == "Banking") {
                    persentage = 2.0
                    commitment = 12
                } else if (OrganizationSelected == "telecommunications") {
                    persentage = 5.0
                    commitment = 48
                } else if (OrganizationSelected == "Technology") {
                    persentage = 9.0
                    commitment = 48
                }

                textView_Cal.setText(OrganizationSelected + "\n"+ persentage+"% Benefit \nfor "+ commitment+" months commitment")

            }
        }
        /////////////////////////////////////////////////////////////////////
        v?.button_invest?.setOnClickListener {

               sendSMS()
            val requestParams = HashMap<String,Any>()

            requestParams["company"] = investmentFragment.OrganizationSelected
            requestParams["amount"] = investmentFragment.amountToInvest
            requestParams["rate"] = investmentFragment.persentage
            requestParams["owner_id"] = 4
            requestParams["duration"]=investmentFragment.commitment

            val paramsJsonObject = JSONObject(requestParams as Map<*, *>)




            var queue = AppSingleton.instance.getQueue(context)

            var addUser = JsonObjectRequest(
                Request.Method.POST, AppSingleton.instance.getBaseUrl()
                    + "invest/add", paramsJsonObject, Response.Listener {


                //                var careList = Gson().toJson()<ArrayList<personWhoCare>>(it,
//                    object: TypeToken<ArrayList<personWhoCare>>(){}.type)
                // Success case
                Log.d("json", it.toString())
//                Log.d("json1",careList.toString())

                Toast.makeText(context, "Successful addition !", Toast.LENGTH_LONG).show()

            }, Response.ErrorListener {
                // Error case
                Log.d("jsonerror",it.toString())
                Toast.makeText(context, "Internet connection error!", Toast.LENGTH_LONG).show()
            })

            queue?.add(addUser)
        }


        return v
    }

    fun sendSMS()
    {
        val smsManager = SmsManager.getDefault() as SmsManager
        smsManager.sendTextMessage("+96599623221", "Boubyan", "Your investment has been accepted", null, null)

    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

      var  items = view!!.resources.getStringArray(R.array.Org_list)
       var lvHomePage = OrgList

        lvHomePage.setAdapter(
            ArrayAdapter<String>(
                view!!.context,
                android.R.layout.simple_list_item_1, items
            )
        )


    }

}