package com.dana.baizaty_frontend.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.dana.baizaty_frontend.Notification.AddCaringPeson
import com.dana.baizaty_frontend.Notification.PeopleWhoCareAdapter
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.singleton.AppSingleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_pwc.view.*
import personWhoCare
import java.util.ArrayList

class PWCFragment : Fragment() {

    override fun onResume() {
        super.onResume()
        vol()

    }

    private lateinit var lv:ListView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                             savedInstanceState: Bundle?): View? {

        var v= inflater.inflate(R.layout.fragment_pwc,null)

         lv=v.findViewById<ListView>(R.id.PeopleWhoCareListView)


        vol()

        v?.AddCareimageButton?.setOnClickListener{
            val intent = Intent(activity, AddCaringPeson::class.java)
            startActivity(intent)
        }

        return v
    }

    fun vol(){
        var queue = AppSingleton.instance.getQueue(activity)

        var careURL = "http://206.189.179.71:3000/peopleWhoCare/4"

        var CareRequest = StringRequest(Request.Method.GET, careURL,
            Response.Listener<String> { response ->

                var careList = Gson().fromJson<ArrayList<personWhoCare>>(response,
                    object: TypeToken<ArrayList<personWhoCare>>(){}.type)

                // it'll send the AnswerList array to the Ingredient Addapter with the fragment Acitvity

                lv.adapter= activity?.let {
                    PeopleWhoCareAdapter(careList, it) }


            }, Response.ErrorListener {
                // Error case
                Log.d("PersonERROR",it.toString())
            })  //push

        queue?.add(CareRequest)
    }

}