package com.dana.baizaty_frontend.fragments

import Transaction
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.anychart.APIlib
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.enums.Align
import com.anychart.enums.LegendLayout
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.singleton.AppSingleton
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_pfm.*


class PFM : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


//        val anyChartView: AnyChartView? = any_chart_view

        // Inflate the layout for this fragment
//        createChart()

        var v = inflater.inflate(R.layout.fragment_pfm, container, false)


        return v


    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        getTransactions()
        button.setOnClickListener{
            updateChart()
        }

    }


    override fun onStart() {


        super.onStart()
        getTransactions()

        Toast.makeText(context, "testing", "testing".length).show()
    }
    //data1: MutableList<DataEntry>
    fun createChart (data1:PieData
    ) {
//        var anyChartView: AnyChartView? = any_chart_view
//
//        APIlib.getInstance().setActiveAnyChartView(any_chart_view)
//        any_chart_view.clear()
////        any_chart_view.visibility=View.GONE
//        any_chart_view.setProgressBar(progress_bar)
//        var pie = AnyChart.pie3d()
//        anyChartView?.setProgressBar(progress_bar)


//        data.add(ValueDataEntry("Restarant", 30))
//        data.add(ValueDataEntry("transportation", 23))
//        data.add(ValueDataEntry("cafe", 10))
//        Log.d("catagor3",data1.toString())
//
//        pie.data(data1)
//        pie.title("spending")
//        pie.labels().position("outside")
//        pie.legend().title().enabled(true)
//
//        pie.legend()
//            .position("center-bottom")
//            .itemsLayout(LegendLayout.HORIZONTAL)
//            .align(Align.CENTER);
//
//        any_chart_view?.setChart(pie);
//        any_chart_view.invalidate()
//
//        any_chart_view.visibility=View.VISIBLE
//        data1.clear()

        //////
        any_chart_view.setUsePercentValues(true)
//                var dataSet =  PieDataSet(ydata as MutableList<PieEntry>, "spending");
//        var piedata= PieData(xdata,dataSet)
any_chart_view.data=data1
        any_chart_view.description.text = ""
        any_chart_view.isDrawHoleEnabled = false

        any_chart_view.invalidate()
        data1.setValueTextSize(13f)
    }
    fun updateChart(){
//
//        fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit()
//        var data1 = mutableListOf<DataEntry>()
//        data1.add(ValueDataEntry("Restarant", 30))
//        data1.add(ValueDataEntry("transportation", 23))
//        data1.add(ValueDataEntry("cafe", 10))
//        var pie=AnyChart.pie3d()
//        any_chart_view.setChart(pie)
        any_chart_view.invalidate()

    }

    fun getTransactions() {
        var data = mutableListOf<DataEntry>()
        var ydata= mutableListOf<PieEntry>()
        var xdata= mutableListOf<String>()
        any_chart_view.invalidate()
        var queue = AppSingleton.instance.getQueue(context)
        var transactionsRequest = StringRequest(
            Request.Method.GET,
            AppSingleton.instance.getBaseUrl() + "transactions/4",
            Response.Listener<String> { response ->
                // Success case

                var transactionList = Gson().fromJson<List<Transaction>>(response, object :
                    TypeToken<List<Transaction>>() {}.type)

                var catagories = mutableListOf<String>()
                var values = mutableListOf<Float>()

                transactionList.forEach {
//                    Log.d("catgor",it.toString())
                    if (catagories.contains(it.catagorie)) {
                        var idx = catagories.indexOf(it.catagorie)
//                        Log.d("catgor1",idx.toString())
//                        Log.d("catgor2",catagories.toString())

                        values[idx] += it.amount.toFloat()

                    }else{
                        catagories.add(it.catagorie)
                        values.add(it.amount.toFloat())
                        var idx=catagories.indexOf(it.catagorie)
//                        values[idx]=0.0
                    }
                }
                Log.d("catagor",catagories.toString())
                Log.d("catagor",values.toString())


                catagories.forEach{
                    var idx=catagories.indexOf(it)
                    ydata.add(PieEntry(values[idx] as Float,catagories[idx].toString() ))
                    xdata.add(catagories[idx])
                    data.add(ValueDataEntry(it,values[idx]))
                }
                Log.d("catagor333",ydata.toString())

                val dataSet = PieDataSet(ydata, "")
                dataSet.setColors(*ColorTemplate.COLORFUL_COLORS)
                val data1 = PieData(dataSet)


                createChart(data1)
                data.clear()
                Log.d("catagor3",data.toString())


            },
            Response.ErrorListener {
                // Error case
            })
        queue?.add(transactionsRequest)
    }
}
