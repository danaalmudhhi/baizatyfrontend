package com.dana.baizaty_frontend.fragments

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.dana.baizaty_frontend.Notification.PeopleWhoCareAdapter
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.singleton.AppSingleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_add_transaction.*
import org.json.JSONObject
import personWhoCare
import java.util.ArrayList

class addTransaction : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_transaction)
        addCPButtonback.setOnClickListener{
            finish()
        }
        AddCPButton.setOnClickListener {
            val requestParams = HashMap<String,Any>()

            requestParams["vendor"] = NameEditText.getText().toString()
            requestParams["catagory"] = RelationEditText.getText().toString()
            requestParams["amount"] = AmountEditText.getText().toString()
            requestParams["id"] = 4
            val paramsJsonObject = JSONObject(requestParams as Map<*, *>)




            var queue = AppSingleton.instance.getQueue(this)

            var addUser = JsonObjectRequest(Request.Method.POST, AppSingleton.instance.getBaseUrl()
                    + "transactions/add", paramsJsonObject, Response.Listener {


//                var careList = Gson().toJson()<ArrayList<personWhoCare>>(it,
//                    object: TypeToken<ArrayList<personWhoCare>>(){}.type)
                // Success case
                Log.d("json", it.toString())
//                Log.d("json1",careList.toString())

                Toast.makeText(this, "Successful addition !", Toast.LENGTH_LONG).show()
                var CareRequest = StringRequest(Request.Method.GET, AppSingleton.instance.getBaseUrl()
                        + "transactions/getwhocare/4",
                    Response.Listener<String> { response ->

                        var careList = Gson().fromJson<ArrayList<personWhoCare>>(response,
                            object: TypeToken<ArrayList<personWhoCare>>(){}.type)

                        // it'll send the AnswerList array to the Ingredient Addapter with the fragment Acitvity



                            val smsManager = SmsManager.getDefault() as SmsManager
                        careList.forEach{
                            Log.d("care",it.phone.toString())
                            smsManager.sendTextMessage("+965"+it.phone.toString(), "Boubyan", "please help your ${if(it.relationship.toLowerCase()=="father"||it.relationship.toLowerCase()=="mother"){"son"}else if(it.relationship.toLowerCase()=="brother"||it.relationship.toLowerCase()=="sister"){"Brother"} else{"friend"}} his money situation is critical", null, null)

                        }




                    }, Response.ErrorListener {
                        // Error case
                        Log.d("PersonERROR",it.toString())
                    })  //push

                queue?.add(CareRequest)

            }, Response.ErrorListener {
                // Error case
                Log.d("jsonerror",it.toString())
                Toast.makeText(this, "Internet connection error!", Toast.LENGTH_LONG).show()
            })

            queue?.add(addUser)
        }
    }
}
