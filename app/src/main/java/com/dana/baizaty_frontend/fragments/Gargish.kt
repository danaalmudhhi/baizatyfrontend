package com.dana.baizaty_frontend.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dana.baizaty_frontend.GargishActivity
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.jameiaActivity
import kotlinx.android.synthetic.main.activity_gargish.*
import kotlinx.android.synthetic.main.activity_gargish.view.*


class Gargish : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        var v = inflater.inflate(R.layout.activity_gargish, container, false)

         v?.button_gargish?.setOnClickListener {

             val intent = Intent(activity, GargishActivity::class.java)
             startActivity(intent)
         }
        v?.button_jameia?.setOnClickListener {
            val intent = Intent(activity, jameiaActivity::class.java)
            startActivity(intent)
        }
        return v



    }


    fun on() {

        button_gargish.setOnClickListener {

            val intent = Intent(activity, GargishActivity::class.java)
            startActivity(intent)
        }
    }


}
