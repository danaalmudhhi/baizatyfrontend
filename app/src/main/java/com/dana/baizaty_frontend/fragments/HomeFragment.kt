package com.dana.baizaty_frontend.fragments

import AccountsSum
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.dana.baizaty_frontend.R
import com.dana.baizaty_frontend.model.Accounts
import com.dana.baizaty_frontend.singleton.AppSingleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

//        AccountsSum()

        // Inflate the layout for this fragment

        var v = inflater.inflate(R.layout.fragment_home, container, false)

//        getAccounts()



        return v
    }


    override fun onResume() {
        super.onResume()
        AccountsSum()
        getAccounts()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTransaction.setOnClickListener{
            startActivity(Intent(context,com.dana.baizaty_frontend.fragments.addTransaction::class.java))
//            getAccounts()
//            AccountsSum()

        }

    }

    fun AccountsSum() {
        var queue = AppSingleton.instance.getQueue(context)
        var AccountsSumRequest = StringRequest(Request.Method.GET, AppSingleton.instance.getBaseUrl() + "accounts/4", Response.Listener<String> { response ->
            // Success case

            var sum = Gson().fromJson(response,AccountsSum::class.java)

            tv_sum.text="KWD"+sum.sum.toString()
            Toast.makeText(context,sum.sum.toString(),sum.sum.toString().length)
        }, Response.ErrorListener {
            // Error case
        })
        queue?.add(AccountsSumRequest)
    }

    fun getAccounts(){
        var queue = AppSingleton.instance.getQueue(context)
        var accountsTrackingRequest = StringRequest(Request.Method.GET, AppSingleton.instance.getBaseUrl() + "accounts/accounts/4", Response.Listener<String> { response ->
            // Success case

            var accountList =Gson().fromJson<List<Accounts>>(response, object :
                TypeToken<List<Accounts>>(){}.type)


            tv_displayMoney1.text=accountList[0].balance.toString()
            tv_accountType.text=accountList[0].type
            tv_displayMoney2.text=accountList[1].balance.toString()
            tv_accountType2.text=accountList[1].type




        }, Response.ErrorListener {
            // Error case
        })
        queue?.add(accountsTrackingRequest)
    }
    }

